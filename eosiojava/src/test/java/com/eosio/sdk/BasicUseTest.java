package com.eosio.sdk;

import com.eosio.sdk.ecc.crypto.ec.EosKeyPair;
import com.eosio.sdk.ecc.crypto.ec.EosPrivateKey;
import com.eosio.sdk.ecc.service.EosService;
import com.eosio.sdk.implementations.ABIProviderImpl;
import com.eosio.sdk.implementations.EosioJavaRpcProviderImpl;
import com.eosio.sdk.implementations.SoftKeySignatureProviderImpl;
import com.eosio.sdk.interfaces.IABIProvider;
import com.eosio.sdk.interfaces.IRPCProvider;
import com.eosio.sdk.interfaces.ISerializationProvider;
import com.eosio.sdk.interfaces.ISignatureProvider;
import com.eosio.sdk.models.rpc.Action;
import com.eosio.sdk.models.rpc.Authorization;
import com.eosio.sdk.models.rpc.response.PushTransactionResponse;
import com.eosio.sdk.serialization.AbiEosSerializationProviderImpl;
import com.eosio.sdk.session.TransactionProcessor;
import com.eosio.sdk.session.TransactionSession;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.fail;

public class BasicUseTest {


    private String endpoint = "https://api.eostribe.io";

    private IRPCProvider rpcProvider;
    private ISerializationProvider serializationProvider;
    private IABIProvider abiProvider;
    private ISignatureProvider signatureProvider;


    @Before
    public void setup() {
        try {
            rpcProvider = new EosioJavaRpcProviderImpl(endpoint);
            serializationProvider = new AbiEosSerializationProviderImpl();
            abiProvider = new ABIProviderImpl(rpcProvider, serializationProvider);
            signatureProvider = new SoftKeySignatureProviderImpl();
            // Generate and import keys:
            EosKeyPair keyPair = EosService.generateKeys();
            EosPrivateKey privateKey = keyPair.getPrivateKey();
            ((SoftKeySignatureProviderImpl)signatureProvider).importKey(privateKey.toString());
        } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }

    @Test
    public void testSignTransaction() {

        TransactionSession session = new TransactionSession(
                serializationProvider,
                rpcProvider,
                abiProvider,
                signatureProvider
        );

        TransactionProcessor processor = session.getTransactionProcessor();

        String jsonData = "{\n" +
                "\"from\": \"person1\",\n" +
                "\"to\": \"person2\",\n" +
                "\"quantity\": \"10.0000 EOS\",\n" +
                "\"memo\" : \"Something\"\n" +
                "}";

        List<Authorization> authorizations = new ArrayList<>();
        authorizations.add(new Authorization("myaccount", "active"));
        List<Action> actions = new ArrayList<>();
        actions.add(new Action("eosio.token", "transfer", authorizations, jsonData));
        try {
            processor.prepare(actions);
            PushTransactionResponse pushTransactionResponse = processor.signAndBroadcast();
            assertNotNull(pushTransactionResponse);
        } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }
}
