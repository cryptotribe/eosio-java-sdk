/**
 * Provides the classes necessary to describe meaningful exceptions that occur during {@link
 * com.eosio.sdk.session.TransactionProcessor} and {@link com.eosio.sdk.session.TransactionSession}
 * implementations like: {@link com.eosio.sdk.error.session.TransactionGetSignatureError}
 */

package com.eosio.sdk.error.session;