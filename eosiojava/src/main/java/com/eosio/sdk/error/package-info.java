/**
 * Provides the classes/constants necessary to describe meaningful exceptions that occur in all processes
 * of eosio-java like:
 * {@link com.eosio.sdk.session.TransactionProcessor} transaction processing flow,
 * {@link com.eosio.sdk.utilities.EOSFormatter} utilities and other processes.
 */

package com.eosio.sdk.error;