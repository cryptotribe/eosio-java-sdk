/**
 * Provides the classes necessary to describe meaningful exceptions that occur during a Serialization Provider implementation like:
 * {@link com.eosio.sdk.error.serialization.SerializeTransactionError}
 */

package com.eosio.sdk.error.serialization;