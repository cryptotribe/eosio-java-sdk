/**
 * Provides the classes necessary for describe meaningful exceptions that occur during an ABI Provider implementation like:
 * {@link com.eosio.sdk.error.abi.GetAbiError}
 */

package com.eosio.sdk.error.abi;