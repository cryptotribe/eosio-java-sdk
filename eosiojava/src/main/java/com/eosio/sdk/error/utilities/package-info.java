/**
 * Provides the classes necessary to describe meaningful exceptions that occur while using
 * eosio-java utilities. like {@link com.eosio.sdk.error.utilities.PEMProcessorError}
 */

package com.eosio.sdk.error.utilities;