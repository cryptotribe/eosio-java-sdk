/**
 * Provides the classes necessary to describe meaningful exceptions that occur during a signature
 * provider implementation like {@link com.eosio.sdk.error.signature.SignTransactionError}
 */

package com.eosio.sdk.error.signature;