/*
 * Copyright (c) 2017-2019 block.one all rights reserved.
 */

package com.eosio.sdk.error.utilities;

import com.eosio.sdk.error.EosioError;
import org.jetbrains.annotations.NotNull;

/**
 * Error that originates from the {@link com.eosio.sdk.utilities.PEMProcessor} class.
 */
public class PEMProcessorError extends EosioError {

    public PEMProcessorError() {
    }

    public PEMProcessorError(@NotNull String message) {
        super(message);
    }

    public PEMProcessorError(@NotNull String message,
            @NotNull Exception exception) {
        super(message, exception);
    }

    public PEMProcessorError(@NotNull Exception exception) {
        super(exception);
    }
}
