/**
 * Provides the classes necessary for describe meaningful exceptions that occur during an PRC Provider implementation like:
 * {@link com.eosio.sdk.error.rpc.GetInfoRpcError}
 */

package com.eosio.sdk.error.rpc;