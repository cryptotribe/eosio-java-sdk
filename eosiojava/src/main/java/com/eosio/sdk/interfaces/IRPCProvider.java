package com.eosio.sdk.interfaces;

import com.eosio.sdk.error.rpc.GetBlockRpcError;
import com.eosio.sdk.error.rpc.GetInfoRpcError;
import com.eosio.sdk.error.rpc.GetRawAbiRpcError;
import com.eosio.sdk.error.rpc.GetRequiredKeysRpcError;
import com.eosio.sdk.error.rpc.PushTransactionRpcError;
import com.eosio.sdk.models.rpc.request.GetBlockRequest;
import com.eosio.sdk.models.rpc.request.GetRawAbiRequest;
import com.eosio.sdk.models.rpc.request.GetRequiredKeysRequest;
import com.eosio.sdk.models.rpc.request.PushTransactionRequest;
import com.eosio.sdk.models.rpc.response.GetBlockResponse;
import com.eosio.sdk.models.rpc.response.GetInfoResponse;
import com.eosio.sdk.models.rpc.response.GetRawAbiResponse;
import com.eosio.sdk.models.rpc.response.GetRequiredKeysResponse;
import com.eosio.sdk.models.rpc.response.PushTransactionResponse;
import org.jetbrains.annotations.NotNull;

/**
 * The interface of an RPC provider.
 */
public interface IRPCProvider {

    /**
     * Returns an object containing various details about the blockchain.
     *
     * @return the latest info/status of a chain.
     * @throws GetInfoRpcError thrown if there are any exceptions/backend errors during the
     * getInfo() process.
     */
    @NotNull
    GetInfoResponse getInfo() throws GetInfoRpcError;

    /**
     * Returns an object containing various details about a specific block on the blockchain.
     *
     * @param getBlockRequest Info of a specific block.
     * @return the info/status of a specific block in the request
     * @throws GetBlockRpcError thrown if there are any exceptions/backend error during the
     * getBlock() process.
     */
    @NotNull
    GetBlockResponse getBlock(GetBlockRequest getBlockRequest) throws GetBlockRpcError;

    /**
     * Gets raw abi for a given contract.
     *
     * @param getRawAbiRequest Info of a specific smart contract.
     * @return the serialized ABI of a smart contract in the request.
     * @throws GetRawAbiRpcError thrown if there are any exceptions/backend error during the
     * getRawAbi() process.
     */
    @NotNull
    GetRawAbiResponse getRawAbi(GetRawAbiRequest getRawAbiRequest) throws GetRawAbiRpcError;

    /**
     * Returns the required keys needed to sign a transaction.
     *
     * @param getRequiredKeysRequest Info to get required keys
     * @return the required keys to sign a transaction
     * @throws GetRequiredKeysRpcError thrown if there are any exceptions/backend error during the
     * getRequiredKeys() process.
     */
    @NotNull
    GetRequiredKeysResponse getRequiredKeys(GetRequiredKeysRequest getRequiredKeysRequest) throws GetRequiredKeysRpcError;

    /**
     * This method expects a transaction in JSON format and will attempt to apply it to the blockchain.
     *
     * @param pushTransactionRequest the transaction to push with signatures.
     * @return the push transaction response
     * @throws PushTransactionRpcError thrown if there are any exceptions/backend error during the
     * pushTransaction() process.
     */
    @NotNull
    PushTransactionResponse pushTransaction(PushTransactionRequest pushTransactionRequest) throws PushTransactionRpcError;
}
