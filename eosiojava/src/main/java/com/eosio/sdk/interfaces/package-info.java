/**
 * Provides interfaces for requisite eosio-java components that are used as parameters in {@link
 * com.eosio.sdk.session.TransactionProcessor}. This includes interfaces for an ABI provider,
 * RPC provider, Serialization provider and Signature provider.
 */

package com.eosio.sdk.interfaces;