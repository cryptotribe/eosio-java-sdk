package com.eosio.sdk.ecc.crypto.ec;

public final class EosKeyPair {

    private EosPrivateKey privateKey;
    private EosPublicKey publicKey;

    public EosKeyPair(EosPrivateKey privateKey, EosPublicKey publicKey) {
        this.privateKey = privateKey;
        this.publicKey = publicKey;
    }

    public EosPrivateKey getPrivateKey() {
        return privateKey;
    }

    public EosPublicKey getPublicKey() {
        return publicKey;
    }
}
