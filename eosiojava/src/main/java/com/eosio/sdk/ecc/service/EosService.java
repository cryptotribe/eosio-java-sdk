package com.eosio.sdk.ecc.service;

import com.eosio.sdk.ecc.crypto.ec.EosKeyPair;
import com.eosio.sdk.ecc.crypto.ec.EosPrivateKey;
import com.eosio.sdk.ecc.crypto.ec.EosPublicKey;
import com.eosio.sdk.ecc.crypto.ec.EcSignature;
import com.eosio.sdk.ecc.crypto.ec.EcDsa;
import com.eosio.sdk.ecc.crypto.digest.Sha256;


public class EosService {


    public static EosKeyPair generateKeys() {
        EosPrivateKey privateKey = new EosPrivateKey();
        EosPublicKey publicKey = privateKey.getPublicKey();
        return new EosKeyPair(privateKey, publicKey);
    }

    public static String signText(String text, EosPrivateKey privateKey) {
        Sha256 digest = Sha256.from(text.getBytes());
        EcSignature signature = EcDsa.sign(digest, privateKey);
        return signature.toString();
    }

    public static String signText(String text, String key) {
        EosPrivateKey privateKey = new EosPrivateKey(key);
        return signText(text, privateKey);
    }

}