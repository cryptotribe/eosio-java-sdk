/**
 * Provides model classes for using on providers like {@link com.eosio.sdk.interfaces.ISignatureProvider},
 * {@link com.eosio.sdk.interfaces.IRPCProvider} and {@link com.eosio.sdk.interfaces.IABIProvider}
 */

package com.eosio.sdk.models;