/**
 * Provides model classes for Signature provider {@link com.eosio.sdk.interfaces.ISignatureProvider}
 */

package com.eosio.sdk.models.signature;