/**
 * Provides request/response model classes for RPC provider {@link com.eosio.sdk.interfaces.IRPCProvider}
 */

package com.eosio.sdk.models.rpc;