/**
 * Provides implementation classes for default implementation of eosio-java provider interfaces like
 * {@link com.eosio.sdk.interfaces.IABIProvider}
 */

package com.eosio.sdk.implementations;