![Java Logo](img/java-logo.png)
# EOSIO SDK for Java ![EOSIO Alpha](https://img.shields.io/badge/EOSIO-Alpha-blue.svg)

[![Software License](https://img.shields.io/badge/license-MIT-lightgrey.svg)](./LICENSE)
![Language Java](https://img.shields.io/badge/Language-Java-yellow.svg)
![](https://img.shields.io/badge/Deployment%20Target-JVM-blue.svg)

All in One EOSIO SDK for Java by Crypto Tribe (former EOS Tribe).
This project have been a compilation of related EOSIO SDK for Java/Android released by Block One:
See original repository: https://github.com/EOSIO/eosio-java

We found Java SDK provided by B1 hard to use in non-Android project.
Hence we have taken all 4 related projects and merged them into one after removing Android dependency.
We have also refactored all source code package names to match Java best practices.
EOS Tribe have also contributed ECC cryptographic source code to generate private keys and do digest. 

The result is 100% Java platform independent SDK library for EOSIO Blockchains.

*All product and company names are trademarks™ or registered® trademarks of their respective holders. 
Use of them does not imply any affiliation with or endorsement by them.*



## Installation

### Prerequisites

* Java JDK 1.8+ (1.7 source compatibility is targeted)
* Gradle 4.10.1+

### Instructions

gradle clean build 

## Helpful Utilities

One of the most complicated and time consuming tasks about encryption can be figuring out how to transform keys into a format that works on the target blockchain.  This library includes two utilities that make that process painless.  The `EOSFormatter` and `PEMProcessor` classes include methods that allow you to convert a PEM or DER encoded public or private key fo and from the standard EOS formats.  The `PEMProcessor` wraps a key and gives you the ability to extract the type, the DER format, the algorithm used to generate the key, and to perform a checksum validation.  The `EOSFormatter` utility converts keys between DER or PEM and the EOS format and formats signatures and transactions into an EOS compliant format as well.  There is an abundance of documentation on the Internet about converting keys and signatures to a DER encoded or PEM (Privacy Enhanced Mail) format (See [PEM](https://tools.ietf.org/html/rfc1421) and [DER](https://www.itu.int/ITU-T/studygroups/com17/languages/X.690-0207.pdf)).  If you can get your key into one of these formats we provide a simple transition to the EOS format.

## Basic Usage

Transactions are instantiated via a `TransactionSession()` which must be configured with a number of providers and a `TransactionProcessor()`, which manipulates and performs actions on a Transaction, prior to use. The code below shows a very barebones flow. Error handling has been omitted for clarity but should be handled in normal usage. (See [Provider Interface Architecture](#provider-interface-architecture) below for more information about providers.)

```java
IRPCProvider rpcProvider = new EosioJavaRpcProviderImpl("https://baseurl.com/v1/");
ISerializationProvider serializationProvider = new AbiEosSerializationProviderImpl();
IABIProvider abiProvider = new ABIProviderImpl(rpcProvider, serializationProvider);
ISignatureProvider signatureProvider = new SoftKeySignatureProviderImpl();

signatureProvider.importKey(privateKeyK1EOS);
// or...
signatureProvider.importKey(privateKeyR1EOS);

TransactionSession session = new TransactionSession(
        serializationProvider,
        rpcProvider,
        abiProvider,
        signatureProvider
);

TransactionProcessor processor = session.getTransactionProcessor();

String jsonData = "{\n" +
        "\"from\": \"person1\",\n" +
        "\"to\": \"person2\",\n" +
        "\"quantity\": \"10.0000 EOS\",\n" +
        "\"memo\" : \"Something\"\n" +
        "}";

List<Authorization> authorizations = new ArrayList<>();
authorizations.add(new Authorization("myaccount", "active"));
List<Action> actions = new ArrayList<>();
actions.add(new Action("eosio.token", "transfer", authorizations, jsonData));

processor.prepare(actions);

PushTransactionResponse pushTransactionResponse = processor.signAndBroadcast();
```

## License

[MIT](./LICENSE)

## Important

See original repository for more documentation and details: 
https://github.com/EOSIO/eosio-java

We have contributed all changes and improvements back to open source community.